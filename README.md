# Natwest Eureka Server #

Eureka Server is an application that holds the information about all client-service applications. Every Micro service will register into the Eureka server and Eureka server knows all the client applications running on each port and IP address. Eureka Server is also known as Discovery Server.
Currently [natwest-config-server](https://bitbucket.org/natwest-hackathon/natwest-config-server), [natwest-sender](https://bitbucket.org/natwest-hackathon/natwest-sender/src/master/), [natwest-receiver](https://bitbucket.org/natwest-hackathon/natwest-receiver/src/master/) are registered with eureka server.


## Heroku Deployment ##
 * https://natwest-eureka-server.herokuapp.com/

